#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QDebug>
#include <QImage>
#include <QPixmap>
#include <QSoundEffect>
#include <QMediaPlayer>
#include <QPainter>
#include <QTimer>
#include <QTime>
#include <win.h>


struct Stone
{
    int thiskind, PosX, PosY;
    QPixmap StoneMap;
    QPushButton *sotneBtn;

};

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    static float widthX;
    static float heigthX;
    static int GridX;
    static int GridY;
    static int GridSize;
    QLabel *Score;
    void paintEvent(QPaintEvent *);

private:
    Ui::Widget *ui;

    Win *win;
    QPainter *painter;
//    QTimer *totalTimer ;
//    QTime  starTime;
//    QTime endTime;

    void drawInitialScene();
    void start();
    void initialStone();
    void initRemove(int i,int j);
    QLabel *BGLabel;

    QPushButton *startBtn;
    QLabel *startTitle;
    QLabel *GridLabel;
    QLabel *noteLabel1;
    QLabel *noteLabel2;
    QLabel *noteLabel3;
    QLabel *noteLabel4;
    QMediaPlayer *effect;

    Stone mStone[8][8];
    Stone *nStone[2];
    void canSwap(int i , int j);
    void drawStone(int i,int j);
    bool canRemove();
    void openBtn();
    void closeBtn();
    void SwapSound();
    void SelectSound();

    bool canH3(int i,int j);
    bool canH4(int i,int j);
    bool canH5(int i,int j);

    bool canV3(int i,int j);
    bool canV4(int i,int j);
    bool canV5(int i,int j);

    void removeH3(int i ,int j,int bottomLayer);
    void removeH4(int i ,int j,int bottomLayer);
    void removeH5(int i ,int j,int bottomLayer);

    void removeV3(int i ,int j,int bottomLayer);
    void removeV4(int i ,int j,int bottomLayer);
    void removeV5(int i ,int j,int bottomLayer);

    void removeXAll(int j);
    void removeYAll(int i);

    void Sleep(int msec);
signals:
    void emit_totalTime(int totalTime);

private slots:
    void ChangeScene();
    void select(int i , int j);
    void Timer();
    void hasShut();


};
#endif // WIDGET_H
