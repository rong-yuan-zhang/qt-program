#ifndef WIN_H
#define WIN_H

#include <QWidget>
#include <QLabel>
#include <QPalette>


class Win : public QWidget
{
    Q_OBJECT
public:
    explicit Win(QWidget *parent = nullptr);
    ~Win();

private:
    QPalette WinPalette;
    QLabel *result;
    void closeEvent ( QCloseEvent * e );

signals:
    void isShut();

public slots:
    void receive_totalTime(int totalTime);
};

#endif // WIN_H
