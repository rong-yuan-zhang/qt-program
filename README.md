### 1 编写目的<br>
此手册的编写目的是为用户提供体验适用于电脑游戏“宝石迷阵（Bejeweled）”的说明文档，读者为普通的用户。 <br>
<br>
### 2 操作方式<br>
用户通过鼠标点击操作，系统判定相邻两次点击所对应的宝石是否能够启用“消除”程序（消除三个及以上种类一致的相邻砖块），若不能则交换失败。每次操作后还会判定是否有能够消除的宝石存在，并自动“消除”可消除的宝石。<br>
<br>
### 3.技术点解析<br>
1.宝石消除期间封锁其他操作(与延时函数联用)<br>
2 .宝石下落添加动画<br>
3 .不可交换的宝石能够自动返回（建立一个能自动清空的容器）<br>
4 .特殊宝石，消除时能够消除所在一整行或一整列<br>
5. 初始界面设置随机方块<br>
6.随机补充方块<br>
5.透明及圆形按钮<br>
6.添加点击音效<br>
7,消除判定算法<br>
8.消除实现算法<br>
9.计时器<br>
10.多窗口间数据传输<br>



