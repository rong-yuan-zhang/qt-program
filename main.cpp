#include "widget.h"
#include "win.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    Win v;
    w.show();
    w.move(300,50);
    w.setFixedSize(1280,960);
    w.setWindowTitle("程序员变强计划");
    return a.exec();
}
