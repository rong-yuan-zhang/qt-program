#include "widget.h"
//#include "win.h"
#include "ui_widget.h"
#include <QPixmap>
#include <QIcon>
#include <QPalette>
#include <QBrush>
#include <QBitmap>
#include <QImage>
#include <QString>
//#include <QTime>
//#include <QTimer>
#include <QRgb>
#include <QFont>


#include <QUrl>
#include <QDir>



/*该项目使用git进行版本管理<Salamander>*/

//静态成员在基类声明时未分配空间，故这里需要
float Widget::widthX;
float Widget::heigthX;
int Widget::GridX = 50;
int Widget::GridY = 50;
int Widget::GridSize = 800;
int TILE_SIZE = Widget::GridSize/8;
bool isFirst = true;
bool isChangeScene = false;
int score=5000;
#include <QPen>
QString baseText="剩余头发数:";
int Timer_id ;
int totalTime ;


int testScore = 4000;
//enum Kind
//{
//    Bule,
//    Yellow,
//    Pink
//}kind;
//QDateTime::fromMSecsSinceEpoch( - START_TIME.toMSecsSinceEpoch()).toString("hh:mm:ss")
Widget::Widget(QWidget *parent)
    : QWidget(parent)
   , ui(new Ui::Widget)
{
    ui->setupUi(this);

    win = new Win();
    //resize(1280,960);  窗口全局设置放在了main中

    widthX = this->width();
    heigthX = this->height();
    drawInitialScene();

    QTimer *timer = new QTimer(this); //this 为parent类, 表示当前窗口
    connect(timer, SIGNAL(timeout()), this, SLOT(Timer())); // SLOT填入一个槽函数
    timer->start(1000); // 1000毫秒, 等于 1 秒
    //qDebug()<< QString("%1/%2").arg(QDir::currentPath()).arg("Keyboard.wav");
    /*Timer_id =totalTime->timerId();
                totalTime->start(); *///开始计时


}

Widget::~Widget()
{
    painter->end();
    delete ui;
}

void Widget::paintEvent(QPaintEvent *QPaintEvent)
{if(isChangeScene)
    {
    painter = new QPainter (this);
    QPen pen(Qt::black, 100, Qt::DashDotLine, Qt::RoundCap, Qt::RoundJoin);

    QFont font;
    font.setFamily("方正基础像素");
    // 大小
    font.setPointSize(70);
    font.setLetterSpacing(QFont::AbsoluteSpacing, 1);
    // 设置加粗
    font.setBold(true);
    // 使用字体
    painter->setFont(font);
       // painter.fillRect(this->rect(), QColor(40, 40, 40));
        painter->setPen(QPen(pen));
        if(score>5000)
            score = 5000;
//        if(score<=4900)
//        {
//            //QDateTime::fromMSecsSinceEpoch(endTime.msec()-starTime.msec() ) .toString("用时mm分ss秒")
//            //qDebug("GG");有效
//            emit emit_totalTime(totalTime);
//            connect(this,&Widget::emit_totalTime,win,&Win::receive_totalTime);
//            win->show();
//            //killTimer(Timer_id);//显示结果);
//        }
//    实际：    if(score<=0)
//        {
//            score = 0;
//            emit emit_totalTime(totalTime);
//            connect(this,&Widget::emit_totalTime,win,&Win::receive_totalTime);
//            win->show();

//        }
        //演示
        if(score<=testScore)
        {
            score = 0;
            connect(this,&Widget::emit_totalTime,win,&Win::receive_totalTime);
            emit emit_totalTime(totalTime);

            connect(win,&Win::isShut,this,&Widget::hasShut);
            win->show();
            this->hide();

        }
        painter->drawText(this->rect(), Qt::AlignRight | Qt::AlignTop, baseText+QString::number(score).toStdString().data());
        //painter->drawText(Score, Qt::AlignCenter,tr("baseText+QString::number(score).toStdString().data()"));
    }
    else
    {

    }
}
void Widget::hasShut()
{
    score=5000;

    this->show();
}

void Widget::SwapSound()
{
    effect=new QMediaPlayer;
    effect->setMedia(QUrl("qrc:/Resources/Audio/Keyboard.wav"));

    effect->play();
}

void Widget::SelectSound()
{
    effect=new QMediaPlayer;
            effect->setMedia(QUrl("qrc:/Resources/Audio/Mouse.wav"));

            effect->play();
}

void Widget::ChangeScene()
{
    SelectSound();
    start();

//    Score = new QLabel(this);
//    Score->setGeometry(860,0,420,100);
//    Score->show();
//    Score->setStyleSheet("background-color:white;font-size:40px;color:Dark");
//    Score->setText(baseText+QString::number(score).toStdString().data());

    noteLabel1 = new QLabel(this);
    noteLabel1->setGeometry(860,200,420,50);
    noteLabel1->show();
    noteLabel1->setStyleSheet("background-color:white;font-size:40px;color:Dark");//透明qRgb(0,0,0)
    noteLabel1->setText("连续敲三段代码掉100根");

    noteLabel2 = new QLabel(this);
    noteLabel2->setGeometry(860,300,420,50);
    noteLabel2->show();
    noteLabel2->setStyleSheet("background-color:white;font-size:40px;color:Dark");
    noteLabel2->setText("连续敲四段代码掉200根");

    noteLabel3 = new QLabel(this);
    noteLabel3->setGeometry(860,400,420,50);
    noteLabel3->show();
    noteLabel3->setStyleSheet("background-color:white;font-size:40px;color:Dark");
    noteLabel3->setText("连续敲五段代码掉500根");

    noteLabel4 = new QLabel(this);
    noteLabel4->setGeometry(860,500,420,50);
    noteLabel4->show();
    noteLabel4->setStyleSheet("background-color:white;font-size:40px;color:Dark");
    noteLabel4->setText("VS编译代码，掉1000根");
    isChangeScene = true;
    //update();
}
void Widget::start()
{
    startTitle->deleteLater();
    startBtn->deleteLater();
    BGLabel->deleteLater();

    QPalette RunningPalette;
    QPixmap RunningBGPix(":/Resources/Image/Blank.jpg");
    //实现图片拉伸，前面必加"QPixmap = "
    RunningBGPix=RunningBGPix.scaled(this->size());
    RunningPalette.setBrush(this->backgroundRole(),QBrush(RunningBGPix));
    this->setPalette(RunningPalette);

    GridLabel = new QLabel(this);
    GridLabel->setGeometry(GridX,GridY,GridSize,GridSize);
    QPixmap GridPix(":/Resources/Image/Grid_05.png");
    GridPix = GridPix.scaled(GridLabel->size());
    GridLabel->setPixmap(GridPix);
    GridLabel->show();
    //生成宝石的函数
    initialStone();

}
void Widget::drawInitialScene()
{
    //背景，先向画布添加图片，再绘制
    QPixmap BGPix(":/Resources/Image/Blank.jpg");
    QPalette BGPalette;
    //BGPix = BGPix.scaled(this->size());
    //实现图片拉伸，前面必加"QPixmap = "
    BGPalette.setBrush(this->backgroundRole(),QBrush(BGPix));
    this->setPalette(BGPalette);

    BGLabel = new QLabel(this);
    BGLabel->setGeometry(0,300,1000,600);
    QPixmap BGPeoplePix(":/Resources/Image/BG3.png");
    BGPeoplePix = BGPeoplePix.scaled(BGLabel->size());
    BGLabel->setPixmap(BGPeoplePix);

    startTitle = new QLabel(this);
    QPixmap TitlePix(":/Resources/Image/Title_3.png");
    TitlePix = TitlePix.scaled(900,300);
    startTitle->setPixmap(TitlePix);
    startTitle->move(330,100);
    startTitle->show();

    //运行按钮，形状更改为圆形
    QPixmap startBtnPix (":/Resources/Image/Start.png");
    startBtn = new QPushButton(this);
    startBtn->setGeometry(1000,700,200,200);//1280/2 - 250/2 = 515居中坐标
    startBtnPix = startBtnPix.scaled(startBtn->width(),startBtn->height(),
                                     Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    startBtn->setIconSize(QSize(startBtn->width(),startBtn->height()));
    startBtn->setIcon(startBtnPix);
    /* 设置按钮的有效区域 */
    startBtn->setMask(QBitmap(startBtnPix.mask()));

    //连接信号与槽函数
    connect(startBtn,&QPushButton::clicked,this,&Widget::ChangeScene);


}



void Widget::initialStone()
{
     //生成棋子，暂设类型为button
    for (int i=0;i<8;i++)
    {
        for (int j=0;j<8;j++)
         {
             mStone[i][j].thiskind = rand()%5;
             //使初始时刻不存在可消除方块
             initRemove(i,j);

             //放在drawStone外，交换时只改kind，可减少内存消耗
             mStone[i][j].PosX = i * TILE_SIZE + GridX;
             mStone[i][j].PosY = j * TILE_SIZE + GridY;
             mStone[i][j].sotneBtn = new QPushButton(this);
             mStone[i][j].sotneBtn->setGeometry(mStone[i][j].PosX+1,mStone[i][j].PosY+1,
                                                    TILE_SIZE,TILE_SIZE);//1280/2 - 250/2 = 515居中坐标

//             mStone[i][j].StoneMap = mStone[i][j].StoneMap.scaled(mStone[i][j].sotneBtn->width(),mStone[i][j].sotneBtn->height(),
//                                              Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
//             mStone[i][j].sotneBtn->setIconSize(QSize(mStone[i][j].sotneBtn->width(),mStone[i][j].sotneBtn->height()));

             mStone[i][j].StoneMap = mStone[i][j].StoneMap.scaled(TILE_SIZE,TILE_SIZE,
                                             Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
             mStone[i][j].sotneBtn->setIconSize(QSize(TILE_SIZE,TILE_SIZE));
            // mStone[i][j].sotneBtn->setMask(QBitmap(mStone[i][j].StoneMap.mask()));

             drawStone(i,j);
             mStone[i][j].sotneBtn->show();
             connect(mStone[i][j].sotneBtn,&QPushButton::clicked,this,[=](){select(i, j );});

             //connect(mStone[i][j].sotneBtn,&QPushButton::clicked,this,&Widget::canSwap);
         }
    }

}

void Widget::drawStone(int i,int j)
{
    if (mStone[i][j].thiskind == 0)
    {
       mStone[i][j].StoneMap.load(":/Resources/Image/C++.png");
      // qDebug()<<"C++";
    }
    else if(mStone[i][j].thiskind == 1)
    {
        mStone[i][j].StoneMap.load(":/Resources/Image/Java.png");
       // qDebug()<<"java";
    }
    else if(mStone[i][j].thiskind == 2)
    {
        mStone[i][j].StoneMap.load(":/Resources/Image/HTML.png");
    }
    else if(mStone[i][j].thiskind == 3)
    {
        mStone[i][j].StoneMap.load(":/Resources/Image/JS.png");
    }
    else if(mStone[i][j].thiskind == 4)
    {
        mStone[i][j].StoneMap.load(":/Resources/Image/python.png");

    }
    else if(mStone[i][j].thiskind == 5)
    {
        mStone[i][j].StoneMap.load(":/Resources/Image/VS.png");

    }
    else if(mStone[i][j].thiskind == 6)
    {
        mStone[i][j].StoneMap.load(":/Resources/Image/NULL.png");      
    }
    mStone[i][j].StoneMap.scaled(TILE_SIZE-2,TILE_SIZE-2);
      mStone[i][j].sotneBtn->setIcon(mStone[i][j].StoneMap);
      mStone[i][j].sotneBtn->setFlat(true);
    /* 设置按钮的有效区域 */
    //mStone[i][j].sotneBtn->setMask(QBitmap(mStone[i][j].StoneMap.mask()));
//    mStone[i][j].sotneBtn->show();

}

bool Widget::canRemove()
 {
    bool canremove = false;
  //横向消除                                                                                                                                                          //    }
    for (int j=0;j<8;j++)
    {
        for (int i=0;i<8;i++)
        {
            if(canH3(i,j)&&mStone[i][j].thiskind != 5)
            {
                int bottomLayer = j;
                canremove = true;
                if(canH4(i,j))
                {
                    if(canH5(i,j))
                    {
                        removeH5(i,j,bottomLayer);
                        //score=score-500;
                  //      Score->setText(baseText+QString::number(score).toStdString().data());

                    }
                    else
                    {
                        removeH4(i,j,bottomLayer);
                        //score=score-200;
                  //      Score->setText(baseText+QString::number(score).toStdString().data());
                    }
                }
                else
                {
                    removeH3(i,j,bottomLayer);
                    //score=score-100;
                  //  Score->setText(baseText+QString::number(score).toStdString().data());
                  //update();


                 }
            }
            else if (canH3(i,j)&&mStone[i][j].thiskind == 5)
            {
               // qDebug("BOss");
                int bottomLayer = j;
                removeXAll(j);
                //score=score-1000;
                //Score->setText(baseText+QString::number(score).toStdString().data());

            }


        }
    }

    //纵向消除
    for (int j=0;j<8;j++)
    {
        for (int i=0;i<8;i++)
         {
            if(canV3(i,j)&&mStone[i][j].thiskind != 5)
            {
                int bottomLayer = j;
                canremove = true;
                if(canV4(i,j))
                {
                    if(canV5(i,j))
                    {
                        removeV5(i,j,bottomLayer);
                        //score=score-500;
                        //Score->setText(baseText+QString::number(score).toStdString().data());
                    }
                    else
                    {
                        removeV4(i,j,bottomLayer);
                        //score=score-200;
                        //Score->setText(baseText+QString::number(score).toStdString().data());
                    }
                }
                 else
                {
                    removeV3(i,j,bottomLayer);
                    //score=score-100;
                    //Score->setText(baseText+QString::number(score).toStdString().data());

                 }
            }
            else if (canV3(i,j)&&mStone[i][j].thiskind == 5)
            {
                removeYAll(i);
                //score=score-1000;
                //Score->setText(baseText+QString::number(score).toStdString().data());

            }


        }
    }

    openBtn();
    return canremove;
}


void Widget::canSwap(int i ,int j)
{   //用到指针，须有中间变量temp
    if(nStone[0]->PosX - nStone[1]->PosX==TILE_SIZE&&nStone[0]->PosY == nStone[1]->PosY)
    {   //qDebug("canSwap_01 is running");//成立
        int temp =nStone[1]->thiskind;
        mStone[i][j].thiskind = nStone[0]->thiskind;
        mStone[i+1][j].thiskind = temp;
        drawStone(i,j);
        drawStone(i+1,j);
       // nStone[0] = nullptr;
        //nStone[1] = nullptr;
        //isFirst = true ;

 //   canRemove();
        if(!canRemove())
        {
            //QThread::sleep(400);
            Sleep(400);
            int temp =nStone[1]->thiskind;
            mStone[i][j].thiskind = nStone[0]->thiskind;
            mStone[i+1][j].thiskind = temp;
            drawStone(i,j);
            drawStone(i+1,j);

        }


    }
    else if(nStone[0]->PosX - nStone[1]->PosX==-TILE_SIZE&&nStone[0]->PosY == nStone[1]->PosY)
    {   int temp =nStone[1]->thiskind;
        mStone[i][j].thiskind = nStone[0]->thiskind;
        mStone[i-1][j].thiskind = temp;
        drawStone(i,j);
        drawStone(i-1,j);
        if(!canRemove())
        {
            //QThread::sleep(400);
            Sleep(400);
            int temp =nStone[1]->thiskind;
            mStone[i][j].thiskind = nStone[0]->thiskind;
            mStone[i-1][j].thiskind = temp;
            drawStone(i,j);
            drawStone(i-1,j);
        }
//        nStone[0] = nullptr;
//        nStone[1] = nullptr;
        //isFirst = true ;
    }
    else if(nStone[0]->PosY - nStone[1]->PosY==TILE_SIZE&&nStone[0]->PosX == nStone[1]->PosX)
    {   int temp =nStone[1]->thiskind;
        mStone[i][j].thiskind = nStone[0]->thiskind;
        mStone[i][j+1].thiskind = temp;
        drawStone(i,j);
        drawStone(i,j+1);
        if(!canRemove())
        {
            //QThread::sleep(400);
            Sleep(400);
            int temp =nStone[1]->thiskind;
            mStone[i][j].thiskind = nStone[0]->thiskind;
            mStone[i][j+1].thiskind = temp;
            drawStone(i,j);
            drawStone(i,j+1);

        }
    }
    else if(nStone[0]->PosY - nStone[1]->PosY==-TILE_SIZE&&nStone[0]->PosX == nStone[1]->PosX)
    {   int temp =nStone[1]->thiskind;
        mStone[i][j].thiskind = nStone[0]->thiskind;
        mStone[i][j-1].thiskind = temp;
        drawStone(i,j);
        drawStone(i,j-1);
        if(!canRemove())
        {
            //QThread::sleep(400);
            Sleep(400);
            int temp =nStone[1]->thiskind;
            mStone[i][j].thiskind = nStone[0]->thiskind;
            mStone[i][j-1].thiskind = temp;
            drawStone(i,j);
            drawStone(i,j-1);

        }

    }
    while (canRemove())
        canRemove();

}

void Widget::initRemove(int i, int j)
{
        if(mStone[i][j].thiskind==mStone[i-1][j].thiskind &&
                mStone[i][j].thiskind==mStone[i-2][j].thiskind &&i>1)
        {
            if(mStone[i][j].thiskind != 4)
                mStone[i][j].thiskind++;
            else
                mStone[i][j].thiskind = 0;
        }
        if(mStone[i][j].thiskind==mStone[i][j-1].thiskind &&
                mStone[i][j].thiskind==mStone[i][j-2].thiskind &&j>1)
        {
            if(mStone[i][j].thiskind != 4)
                mStone[i][j].thiskind++;
            else
                mStone[i][j].thiskind = 0;
        }


}



void Widget::select(int i ,int j)
{
    SelectSound();
    if(isFirst)
        {
        nStone[0] = &mStone[i][j];
        //qDebug("%s",QString::number(nStone[0]->thiskind).toStdString().data());
        isFirst = false;

    }
    else
        {
        nStone[1] = &mStone[i][j];
        canSwap(i,j);
        nStone[0] = nullptr;
        nStone[1] = nullptr;
        isFirst = true;

    }
        //qDebug("%s",QString::number(i).toStdString().data());//输出横坐标，成功
}


void Widget::Sleep(int msec)
{
    closeBtn();
    QTime dieTime = QTime::currentTime().addMSecs(msec);
    while( QTime::currentTime() < dieTime )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 500);

}


void Widget::openBtn()
{
    for (int j=0;j<8;j++)
    {
        for (int i=0;i<8;i++)
         {mStone[i][j].sotneBtn->setAttribute(Qt::WA_TransparentForMouseEvents, false);

        }
    }
}

void Widget::closeBtn()
{
    for (int j=0;j<8;j++)
    {
        for (int i=0;i<8;i++)
         {mStone[i][j].sotneBtn->setAttribute(Qt::WA_TransparentForMouseEvents, true);

        }
    }
}


bool Widget::canH3(int i,int j)
{
   if ((mStone[i][j].thiskind==mStone[i+1][j].thiskind) &&
                        (mStone[i][j].thiskind==mStone[i+2][j].thiskind)&&i<6 )
        return true;
   else
        return false;
}

bool Widget::canH4(int i,int j)
{
    if ((mStone[i][j].thiskind==mStone[i+1][j].thiskind) &&
                         (mStone[i][j].thiskind==mStone[i+2][j].thiskind)&&
            (mStone[i][j].thiskind==mStone[i+3][j].thiskind)&&i<5 )
         return true;
    else
         return false;
}

bool Widget::canH5(int i,int j)
{
    if ((mStone[i][j].thiskind==mStone[i+1][j].thiskind) &&
                         (mStone[i][j].thiskind==mStone[i+2][j].thiskind)&&
            (mStone[i][j].thiskind==mStone[i+3][j].thiskind)&&
            (mStone[i][j].thiskind==mStone[i+4][j].thiskind)&&i<4 )
         return true;
    else
         return false;
}

bool Widget::canV3(int i,int j)
{
    if((mStone[i][j].thiskind==mStone[i][j+1].thiskind) &&
                        (mStone[i][j].thiskind==mStone[i][j+2].thiskind) &&j<6)
        return true;
   else
        return false;
}

bool Widget::canV4(int i,int j)
{
    if((mStone[i][j].thiskind==mStone[i][j+1].thiskind) &&
                        (mStone[i][j].thiskind==mStone[i][j+2].thiskind) &&
            (mStone[i][j].thiskind==mStone[i][j+3].thiskind)&&j<5)
        return true;
   else
        return false;
}
bool Widget::canV5(int i,int j)
{
    if((mStone[i][j].thiskind==mStone[i][j+1].thiskind) &&
            (mStone[i][j].thiskind==mStone[i][j+2].thiskind) &&
            (mStone[i][j].thiskind==mStone[i][j+3].thiskind)&&
            (mStone[i][j].thiskind==mStone[i][j+4].thiskind)&&j<4)
        return true;
   else
        return false;
}

void Widget::removeH3(int i, int j,int bottomLayer)
{   SwapSound();
    score=score-100;
    update();
    if(bottomLayer>0)
    {
        for(bottomLayer;bottomLayer>0;bottomLayer--)
        {
            Sleep(400);
           // SwapSound();
            mStone[i][bottomLayer].thiskind =  mStone[i][bottomLayer-1].thiskind;
            mStone[i+1][bottomLayer].thiskind =  mStone[i+1][bottomLayer-1].thiskind;
            mStone[i+2][bottomLayer].thiskind =  mStone[i+2][bottomLayer-1].thiskind;
            mStone[i][bottomLayer-1].thiskind =6;
            mStone[i+1][bottomLayer-1].thiskind =6;
            mStone[i+2][bottomLayer-1].thiskind =6;

            drawStone(i,bottomLayer);
            drawStone(i+1,bottomLayer);
            drawStone(i+2,bottomLayer);
            drawStone(i,bottomLayer-1);
            drawStone(i+1,bottomLayer-1);
            drawStone(i+2,bottomLayer-1);
            if(bottomLayer == 1)
              {
                Sleep(200);
                //SwapSound();
                   mStone[i][0].thiskind = rand()%6;
                   mStone[i+1][0].thiskind = (mStone[i][0].thiskind + 3)%6;
                   mStone[i+2][0].thiskind = (mStone[i][0].thiskind + 4)%6;

                   drawStone(i,0);
                   drawStone(i+1,0);
                   drawStone(i+2,0);
                }

         }
    }
    else if(bottomLayer == 0)
    {
        Sleep(200);
        //SwapSound();
         mStone[i][0].thiskind = rand()%6;
         mStone[i+1][0].thiskind = (mStone[i][0].thiskind + 3)%6;
         mStone[i+2][0].thiskind = (mStone[i][0].thiskind + 4)%6;

         drawStone(i,0);
         drawStone(i+1,0);
         drawStone(i+2,0);

    }
}
void Widget::removeH4(int i, int j,int bottomLayer)
{   SwapSound();
    score=score-200;
    update();
    for(bottomLayer;bottomLayer>0;bottomLayer--)
    {

        Sleep(400);
        //SwapSound();
        mStone[i][bottomLayer].thiskind =  mStone[i][bottomLayer-1].thiskind;
        mStone[i+1][bottomLayer].thiskind =  mStone[i+1][bottomLayer-1].thiskind;
        mStone[i+2][bottomLayer].thiskind =  mStone[i+2][bottomLayer-1].thiskind;
        mStone[i+3][bottomLayer].thiskind =  mStone[i+3][bottomLayer-1].thiskind;
        mStone[i][bottomLayer-1].thiskind =6;
        mStone[i+1][bottomLayer-1].thiskind =6;
        mStone[i+2][bottomLayer-1].thiskind =6;
        mStone[i+3][bottomLayer-1].thiskind =6;

        drawStone(i,bottomLayer);
        drawStone(i+1,bottomLayer);
        drawStone(i+2,bottomLayer);
        drawStone(i+3,bottomLayer);
        drawStone(i,bottomLayer-1);
        drawStone(i+1,bottomLayer-1);
        drawStone(i+2,bottomLayer-1);
        drawStone(i+3,bottomLayer-1);
        if(bottomLayer == 1)
          {
            Sleep(400);
            //SwapSound();
               mStone[i][0].thiskind = rand()%5;
               mStone[i+1][0].thiskind = (mStone[i][0].thiskind + 3)%6;
               mStone[i+2][0].thiskind = (mStone[i][0].thiskind + 4)%6;
               mStone[i+3][0].thiskind = (mStone[i][0].thiskind + 1)%6;

               drawStone(i,0);
               drawStone(i+1,0);
               drawStone(i+2,0);
               drawStone(i+3,0);
            }

     }
}

void Widget::removeH5(int i, int j,int bottomLayer)
{   SwapSound();
    score=score-500;
    update();
    for(bottomLayer;bottomLayer>0;bottomLayer--)
    {
        Sleep(400);
        //SwapSound();
        mStone[i][bottomLayer].thiskind =  mStone[i][bottomLayer-1].thiskind;
        mStone[i+1][bottomLayer].thiskind =  mStone[i+1][bottomLayer-1].thiskind;
        mStone[i+2][bottomLayer].thiskind =  mStone[i+2][bottomLayer-1].thiskind;
        mStone[i+3][bottomLayer].thiskind =  mStone[i+3][bottomLayer-1].thiskind;
        mStone[i+4][bottomLayer].thiskind =  mStone[i+4][bottomLayer-1].thiskind;
        mStone[i][bottomLayer-1].thiskind =6;
        mStone[i+1][bottomLayer-1].thiskind =6;
        mStone[i+2][bottomLayer-1].thiskind =6;
        mStone[i+3][bottomLayer-1].thiskind =6;
        mStone[i+4][bottomLayer-1].thiskind =6;

        drawStone(i,bottomLayer);
        drawStone(i+1,bottomLayer);
        drawStone(i+2,bottomLayer);
        drawStone(i+3,bottomLayer);
        drawStone(i+4,bottomLayer);
        drawStone(i,bottomLayer-1);
        drawStone(i+1,bottomLayer-1);
        drawStone(i+2,bottomLayer-1);
        drawStone(i+3,bottomLayer-1);
        drawStone(i+4,bottomLayer-1);
        if(bottomLayer == 1)
          {
            Sleep(400);
            //SwapSound();
               mStone[i][0].thiskind = rand()%5;
               mStone[i+1][0].thiskind = (mStone[i][0].thiskind + 3)%6;
               mStone[i+2][0].thiskind = (mStone[i][0].thiskind + 4)%6;
               mStone[i+3][0].thiskind = (mStone[i][0].thiskind + 1)%6;
               mStone[i+4][0].thiskind = (mStone[i][0].thiskind + 2)%6;

               drawStone(i,0);
               drawStone(i+1,0);
               drawStone(i+2,0);
               drawStone(i+3,0);
               drawStone(i+4,0);
            }

     }
}

void Widget::removeV3(int i, int j,int bottomLayer)
{   SwapSound();
    score=score-100;
    update();
    for(bottomLayer;bottomLayer>2;bottomLayer = bottomLayer-3)
    {
        Sleep(400);
        //SwapSound();


           mStone[i][bottomLayer].thiskind =  mStone[i][bottomLayer-3].thiskind;
           mStone[i][bottomLayer+1].thiskind =  mStone[i][bottomLayer-2].thiskind;
           mStone[i][bottomLayer+2].thiskind =  mStone[i][bottomLayer-1].thiskind;
           mStone[i][bottomLayer-1].thiskind = 6;
           mStone[i][bottomLayer-2].thiskind = 6;
           mStone[i][bottomLayer-3].thiskind = 6;


           drawStone(i,bottomLayer);
           drawStone(i,bottomLayer+1);
           drawStone(i,bottomLayer+2);


            drawStone(i,bottomLayer-1);
            drawStone(i,bottomLayer-2);
            drawStone(i,bottomLayer-3);

       }

    Sleep(400);
    //SwapSound();
    for(bottomLayer;bottomLayer >= 0;bottomLayer--)
    {
        if(bottomLayer > 0)
        {
            mStone[i][bottomLayer+2].thiskind =  mStone[i][bottomLayer-1].thiskind;
            drawStone(i,bottomLayer+2);
        }

        if(bottomLayer == 0)
        {//SwapSound();
            mStone[i][2].thiskind = 6;
            mStone[i][1].thiskind = 6;
            mStone[i][0].thiskind = 6;
            drawStone(i,2);
            drawStone(i,1);
            drawStone(i,0);

            mStone[i][2].thiskind = rand()%6;
            mStone[i][1].thiskind = (mStone[i][2].thiskind + 3)%6;
            mStone[i][0].thiskind = (mStone[i][2].thiskind + 4)%6;
            Sleep(200);
            drawStone(i,2);

            drawStone(i,1);

            drawStone(i,0);
        }


    }
}

void Widget::removeV4(int i, int j,int bottomLayer)
{   SwapSound();
    score=score-200;
    update();
    for(bottomLayer;bottomLayer>3;bottomLayer = bottomLayer-4)
    {Sleep(400);
        //SwapSound();


           mStone[i][bottomLayer].thiskind =  mStone[i][bottomLayer-4].thiskind;
           mStone[i][bottomLayer+1].thiskind =  mStone[i][bottomLayer-3].thiskind;
           mStone[i][bottomLayer+2].thiskind =  mStone[i][bottomLayer-2].thiskind;
           mStone[i][bottomLayer+3].thiskind =  mStone[i][bottomLayer-1].thiskind;
           mStone[i][bottomLayer-1].thiskind = 6;
           mStone[i][bottomLayer-2].thiskind = 6;
           mStone[i][bottomLayer-3].thiskind = 6;
           mStone[i][bottomLayer-4].thiskind = 6;


           drawStone(i,bottomLayer);
           drawStone(i,bottomLayer+1);
           drawStone(i,bottomLayer+2);
           drawStone(i,bottomLayer+3);


            drawStone(i,bottomLayer-1);
            drawStone(i,bottomLayer-2);
            drawStone(i,bottomLayer-3);
            drawStone(i,bottomLayer-4);


       }

    Sleep(400);
    for(bottomLayer;bottomLayer >= 0;bottomLayer--)
    {  //SwapSound();
        if(bottomLayer > 0)
        {
            mStone[i][bottomLayer+3].thiskind =  mStone[i][bottomLayer-1].thiskind;
            drawStone(i,bottomLayer+3);
        }

        if(bottomLayer == 0)
        {
            mStone[i][3].thiskind = 6;
            mStone[i][2].thiskind = 6;
            mStone[i][1].thiskind = 6;
            mStone[i][0].thiskind = 6;

            drawStone(i,3);
            drawStone(i,2);
            drawStone(i,1);
            drawStone(i,0);

            mStone[i][2].thiskind = rand()%6;
            mStone[i][1].thiskind = (mStone[i][2].thiskind + 3)%6;
            mStone[i][0].thiskind = (mStone[i][2].thiskind + 4)%6;
            mStone[i][3].thiskind = (mStone[i][2].thiskind + 1)%6;
            Sleep(400);
            //wapSound();
            drawStone(i,3);
            drawStone(i,2);

            drawStone(i,1);

            drawStone(i,0);
        }
    }
}

void Widget::removeV5(int i, int j,int bottomLayer)
{   SwapSound();
    score=score-500;
    update();
    for(bottomLayer;bottomLayer>4;bottomLayer = bottomLayer-5)
    {   Sleep(400);
        //SwapSound();

           mStone[i][bottomLayer].thiskind =  mStone[i][bottomLayer-5].thiskind;
           mStone[i][bottomLayer].thiskind =  mStone[i][bottomLayer-4].thiskind;
           mStone[i][bottomLayer+1].thiskind =  mStone[i][bottomLayer-3].thiskind;
           mStone[i][bottomLayer+2].thiskind =  mStone[i][bottomLayer-2].thiskind;
           mStone[i][bottomLayer+3].thiskind =  mStone[i][bottomLayer-1].thiskind;
           mStone[i][bottomLayer-1].thiskind = 6;
           mStone[i][bottomLayer-2].thiskind = 6;
           mStone[i][bottomLayer-3].thiskind = 6;
           mStone[i][bottomLayer-4].thiskind = 6;
           mStone[i][bottomLayer-5].thiskind = 6;


           drawStone(i,bottomLayer);
           drawStone(i,bottomLayer+1);
           drawStone(i,bottomLayer+2);
           drawStone(i,bottomLayer+3);
           drawStone(i,bottomLayer+4);


            drawStone(i,bottomLayer-1);
            drawStone(i,bottomLayer-2);
            drawStone(i,bottomLayer-3);
            drawStone(i,bottomLayer-4);
            drawStone(i,bottomLayer-5);


       }

    Sleep(400);
    for(bottomLayer;bottomLayer >= 0;bottomLayer--)
    {   //SwapSound();
        if(bottomLayer > 0)
        {
            mStone[i][bottomLayer+4].thiskind =  mStone[i][bottomLayer-1].thiskind;
            drawStone(i,bottomLayer+4);
        }

        if(bottomLayer == 0)
        {  Sleep(400);
            //SwapSound();
            mStone[i][4].thiskind = 6;
            mStone[i][3].thiskind = 6;
            mStone[i][2].thiskind = 6;
            mStone[i][1].thiskind = 6;
            mStone[i][0].thiskind = 6;
            drawStone(i,4);
            drawStone(i,3);
            drawStone(i,2);
            drawStone(i,1);
            drawStone(i,0);

            mStone[i][2].thiskind = rand()%6;
            mStone[i][1].thiskind = (mStone[i][2].thiskind + 3)%6;
            mStone[i][0].thiskind = (mStone[i][2].thiskind + 4)%6;
            mStone[i][3].thiskind = (mStone[i][2].thiskind + 1)%6;
            mStone[i][4].thiskind = (mStone[i][2].thiskind + 1)%6;

             drawStone(i,4);
            drawStone(i,3);
            drawStone(i,2);

            drawStone(i,1);

            drawStone(i,0);
        }
    }
}

void Widget::removeXAll(int j)
{       SwapSound();
        score=score-1000;
        update();
        int bottomLayer=j;
        if(bottomLayer>0)
        {
            for(bottomLayer;bottomLayer>0;bottomLayer--)
            {
                Sleep(400);
                //SwapSound();


                    for(int x=0;x<8;x++)
                    {

                    mStone[x][bottomLayer].thiskind =  mStone[x][bottomLayer-1].thiskind;
                    //qDebug("%s",QString::number(x).toStdString().data());

                    mStone[x][bottomLayer-1].thiskind =6;

                    drawStone(x,bottomLayer);
                    drawStone(x,bottomLayer-1);
                    }

                if(bottomLayer == 1)
                  {
                        Sleep(200);
                        //SwapSound();
                        for(int x=0;x<8;x++)
                        {
                       mStone[x][0].thiskind = rand()%6;

                       drawStone(x,0);
                        }

                    }


             }
        }
        else if(bottomLayer == 0)
        {    Sleep(200);
            for(int x=0;x<8;x++)
            {
           mStone[x][0].thiskind = rand()%6;

           drawStone(x,0);
            }
          }
}

void Widget::removeYAll(int i)
{   SwapSound();
    score=score-1000;
    update();
    for(int y=0;y<8;y++)
    {
        mStone[i][y].thiskind=6;
        drawStone(i,y);
    }
    //SwapSound();
    Sleep(300);
    for(int y=7;y>=0;y--)
    {
        mStone[i][y].thiskind=rand()%5;
        drawStone(i,y);
    }
}
void Widget::Timer()
{ totalTime++;
//   实际： if(score<5000&&score>0)
//     {
//        score = score +10;
//        update();
//    }
   if(score<5000&&score>testScore)
    {
       score = score +10;
       update();
   }
}










