#include "win.h"
#include <QString>
#include <QDebug>

int T_totalTime;
QString leftText = "用时";
QString rightText = "天";
Win::Win(QWidget *parent)
    : QWidget{parent}
{
this->setWindowTitle("终于。。。");
    QPixmap WinPix(":/Resources/Image/win.png");
    QPalette WinPalette;
    WinPix = WinPix.scaled(638,838);
    this->setFixedSize(WinPix.size());
    //BGPix = BGPix.scaled(this->size());
    //实现图片拉伸，前面必加"QPixmap = "
    WinPalette.setBrush(this->backgroundRole(),QBrush(WinPix));
    this->setPalette(WinPalette);

    result = new QLabel(this);
    result->setGeometry(0,0,200,50);

                result->setStyleSheet("background-color:Dark;font-size:40px;color:white");//透明qRgb(0,0,0)
                result->setText(leftText+QString::number(T_totalTime).toStdString().data()+rightText);
                result->show();

}
void Win::receive_totalTime(int R_totalTime)
{
    T_totalTime = R_totalTime;
    qDebug()<<T_totalTime;
    result->setText(leftText+QString::number(T_totalTime).toStdString().data()+rightText);
}
Win::~Win()
{

    delete this;

}

void Win::closeEvent(QCloseEvent *e)
{
emit isShut();
}
